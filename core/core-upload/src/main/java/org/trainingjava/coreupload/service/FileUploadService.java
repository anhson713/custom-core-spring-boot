package org.trainingjava.coreupload.service;

import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface FileUploadService {

    String save(MultipartFile multipartFile);

    void delete(String fileName);

    InputStreamResource get(String fileName);
}
