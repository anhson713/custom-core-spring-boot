package org.trainingjava.coreupload.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.InputStreamResource;
import org.springframework.web.multipart.MultipartFile;
import org.trainingjava.coreexception.InternalServerException;
import org.trainingjava.coreupload.exception.FileNameInvalidException;
import org.trainingjava.coreupload.service.FileUploadService;

import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

@Slf4j
public class FileUploadServiceImpl implements FileUploadService {

    private final String pathFile;
    private final Path fileStorageLocation;

    public FileUploadServiceImpl(String pathFileStorage) {
        this.pathFile = pathFileStorage;
        this.fileStorageLocation = Paths.get(pathFileStorage).normalize();
        createLocation(fileStorageLocation);
    }

    @Override
    public String save(MultipartFile multipartFile) {
        log.info("(save)multipartFile : {}", multipartFile);
        var fileName = multipartFile.getOriginalFilename();
        validateFileName(fileName);
        fileName = pathFile + '\\' + UUID.randomUUID() + '.' + getExtensionFileName(multipartFile.getOriginalFilename());

        try {
            Path targetLocation = fileStorageLocation.resolve(fileName);
            Files.copy(multipartFile.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
            return fileName;
        } catch (Exception ex) {
            log.error("(save)exception : {} --> INTERNAL SERVER ERROR", ex.getClass().getSimpleName());
            throw new InternalServerException();
        }
    }

    private void validateFileName(String fileName) {
        log.info("(validateFileName)fileName : {}", fileName);
        if (!fileName.contains(".")) {
            log.error("(validateFileName)filename : {} --> FileNameInvalidException", fileName);
            throw new FileNameInvalidException();
        }
    }

    @Override
    public void delete(String fileName) {
        log.info("(delete)fileName : {}", fileName);
        try {
            Files.delete(Paths.get(fileName));
        } catch (Exception e) {
            log.error("(delete)e : {}", e.getMessage());
            throw new InternalServerException();
        }
    }

    @Override
    public InputStreamResource get(String fileName) {
        log.info("(get)fileName : {}", fileName);
        try {
            File file = new File(fileName);
            return new InputStreamResource(new FileInputStream(file));
        } catch (Exception e) {
            log.error("(get)e : {}", e.getMessage());
            throw new InternalServerException();
        }
    }

    private void createLocation(Path fileStorageLocation) {
        log.info("(createLocation)fileStorageLocation : {}", fileStorageLocation);
        try {
            Files.createDirectories(fileStorageLocation);
        } catch (Exception exception) {
            log.error("(createLocation)fileStorageLocation : {}, exception : {}",
                    fileStorageLocation, exception.getClass().getName());
            throw new InternalServerException();
        }
    }

    private String getExtensionFileName(String originalFileName) {
        log.info("(getExtensionFileName)originalFileName : {}", originalFileName);
        String[] fileNameSplit = originalFileName.split("\\.");
        return fileNameSplit[fileNameSplit.length -1];
    }
}
