package org.trainingjava.coreupload.exception;

import org.trainingjava.coreexception.BadRequestException;

public class FileNameInvalidException extends BadRequestException {

    public FileNameInvalidException() {
        setCode("org.trainingjava.coreupload.exception.FileNameInvalidException");
    }
}
