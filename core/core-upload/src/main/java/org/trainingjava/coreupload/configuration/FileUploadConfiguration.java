package org.trainingjava.coreupload.configuration;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.trainingjava.coreupload.service.FileUploadService;
import org.trainingjava.coreupload.service.impl.FileUploadServiceImpl;

@Configuration
@RequiredArgsConstructor
public class FileUploadConfiguration {

    @Value("${application.path.file-storage}")
    private String fileStorageLocation;

    @Bean
    public FileUploadService fileUploadService() {
        return new FileUploadServiceImpl(fileStorageLocation);
    }
}
