package org.aibles.coreredis.service.impl;

import org.aibles.coreredis.service.BaseRedisListService;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.List;


public class BaseRedisListServiceImpl<T> implements BaseRedisListService<T> {

    private final ListOperations<String, T> listOperations;

    public BaseRedisListServiceImpl(RedisTemplate<String, T> redisTemplate) {
        this.listOperations = redisTemplate.opsForList();
    }

    @Override
    public void leftPush(String key, T value) {
        listOperations.leftPush(key, value);
    }

    @Override
    public T leftPop(String key) {
        return listOperations.leftPop(key);
    }

    @Override
    public void leftPushAll(String key, List<T> values) {
        listOperations.leftPushAll(key, values);
    }

    @Override
    public List<T> leftPop(String key, long count) {
        return listOperations.leftPop(key, count);
    }

    @Override
    public void rightPush(String key, T value) {
        listOperations.rightPush(key, value);
    }

    @Override
    public T rightPop(String key) {
        return listOperations.rightPop(key);
    }

    @Override
    public void rightPushAll(String key, List<T> values) {
        listOperations.rightPushAll(key, values);
    }

    @Override
    public List<T> rightPop(String key, long count) {
        return listOperations.rightPop(key, count);
    }

    @Override
    public Long size(String key) {
        return listOperations.size(key);
    }

    @Override
    public List<T> range(String key, long start, long end) {
        return listOperations.range(key, start, end);
    }

    @Override
    public void trim(String key, long start, long end) {
        listOperations.trim(key, start, end);
    }

    @Override
    public T index(String key, long index) {
        return listOperations.index(key, index);
    }
}
