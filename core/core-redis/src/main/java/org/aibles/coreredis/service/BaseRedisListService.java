package org.aibles.coreredis.service;

import java.util.List;

public interface BaseRedisListService<T> {

    void leftPush(String key, T value);

    T leftPop(String key);

    void leftPushAll(String key, List<T> values);

    List<T> leftPop(String key, long count);

    void rightPush(String key, T value);

    T rightPop(String key);

    void rightPushAll(String key, List<T> values);

    List<T> rightPop(String key, long count);

    Long size(String key);

    List<T> range(String key, long start, long end);

    void trim(String key, long start, long end);

    T index(String key, long index);
}
