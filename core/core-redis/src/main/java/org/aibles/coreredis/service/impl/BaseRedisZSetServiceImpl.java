package org.aibles.coreredis.service.impl;

import org.aibles.coreredis.service.BaseRedisZSetService;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;

import java.util.Set;

public class BaseRedisZSetServiceImpl<T> implements BaseRedisZSetService<T> {

    private final ZSetOperations<String, T> zSetOperations;

    public BaseRedisZSetServiceImpl(RedisTemplate<String, T> redisTemplate) {
        this.zSetOperations = redisTemplate.opsForZSet();
    }

    @Override
    public void add(String key, T value, double score) {
        zSetOperations.add(key, value, score);
    }

    @Override
    public Set<T> difference(String key, String otherKey) {
        return zSetOperations.difference(key, otherKey);
    }

    @Override
    public Set<T> intersect(String key, String otherKey) {
        return zSetOperations.intersect(key, otherKey);
    }

    @Override
    public Set<T> union(String key, String otherKey) {
        return zSetOperations.union(key, otherKey);
    }

    @Override
    public long count(String key, double min, double max) {
        return zSetOperations.count(key, min, max);
    }

    @Override
    public void incrementScore(String key, T value, double delta) {
        zSetOperations.incrementScore(key, value, delta);
    }

    @Override
    public long size(String key) {
        return zSetOperations.size(key);
    }

    @Override
    public Set<T> range(String key, long start, long end) {
        return zSetOperations.range(key, start, end);
    }

    @Override
    public Set<T> rangeByScore(String key, double min, double max) {
        return zSetOperations.rangeByScore(key, min, max);
    }

    @Override
    public Set<T> reserveRange(String key, long start, long end) {
        return zSetOperations.reverseRange(key, start, end);
    }

    @Override
    public Set<T> reserveRangeByScore(String key, double min, double max) {
        return zSetOperations.reverseRangeByScore(key, min, max);
    }

    @Override
    public long rank(String key, T value) {
        return zSetOperations.rank(key, value);
    }

    @Override
    public long reserveRank(String key, T value) {
        return zSetOperations.reverseRank(key, value);
    }

    @Override
    public void removeRange(String key, long start, long end) {
        zSetOperations.removeRange(key, start, end);
    }

    @Override
    public void removeRangeByScore(String key, double min, double max) {
        zSetOperations.removeRangeByScore(key, min, max);
    }

    @Override
    public long zCard(String key) {
        return zSetOperations.zCard(key);
    }


}
