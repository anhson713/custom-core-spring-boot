package org.aibles.coreredis.service;

import java.util.List;
import java.util.Set;

public interface BaseRedisZSetService<T> {

    void add(String key, T value, double score);

    Set<T> difference(String key, String otherKey);

    Set<T> intersect(String key, String otherKey);

    Set<T> union(String key, String otherKey);

    long count(String key, double min, double max);

    void incrementScore(String key, T value, double delta);

    long size(String key);

    Set<T> range(String key, long start, long end);

    Set<T> rangeByScore(String key, double min, double max);

    Set<T> reserveRange(String key, long start, long end);

    Set<T> reserveRangeByScore(String key, double min, double max);

    long rank(String key, T value);

    long reserveRank(String key, T value);

    void removeRange(String key, long start, long end);

    void removeRangeByScore(String key, double min, double max);

    long zCard(String key);
}
