package org.aibles.coreredis.service;

public interface BaseRedisValueService<T> {

    void set(String key, T object);

    T get(String key);

    void delete(String key);

    boolean isExist(String key);

    void increase(String key);

    void decrease(String key);

    void increase(String key, long delta);

    void decrease(String key, long delta);


}
