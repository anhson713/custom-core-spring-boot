package org.aibles.coreredis.service.impl;

import org.aibles.coreredis.service.BaseRedisValueService;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import java.util.concurrent.TimeUnit;


public abstract class BaseRedisValueServiceImpl<T> implements BaseRedisValueService<T> {

    private final ValueOperations<String, T> valueOperations;

    private final long timeToLive;

    private final TimeUnit timeUnit;


    protected BaseRedisValueServiceImpl(RedisTemplate<String, T> redisTemplate, long timeToLive, TimeUnit timeUnit) {
        this.valueOperations = redisTemplate.opsForValue();
        this.timeToLive = timeToLive;
        this.timeUnit = timeUnit;
    }

    public abstract boolean isSavePersistent();

    @Override
    public void set(String key, T object) {
        if (isSavePersistent()) {
            valueOperations.set(key, object, timeToLive, timeUnit);
        }
        valueOperations.set(key, object);
    }

    @Override
    public T get(String key) {
        return valueOperations.get(key);
    }

    @Override
    public void delete(String key) {
        valueOperations.getOperations().delete(key);
    }

    @Override
    public boolean isExist(String key) {
        return Boolean.TRUE.equals(valueOperations.getOperations().hasKey(key));
    }

    @Override
    public void increase(String key) {
        validateValueNumber(key);
        valueOperations.increment(key);
    }

    @Override
    public void decrease(String key) {
        validateValueNumber(key);
        valueOperations.decrement(key);
    }

    @Override
    public void increase(String key, long delta) {
        validateValueNumber(key);
        valueOperations.increment(key, delta);
    }

    @Override
    public void decrease(String key, long delta) {
        validateValueNumber(key);
        valueOperations.decrement(key, delta);
    }

    private void validateValueNumber(String key) {
        try {
            Long value = (Long) get(key);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
