package org.aibles.coreredis.service;

import java.util.List;
import java.util.Set;

public interface BaseRedisSetService<T> {

    void add(String key, T value);

    Set<T> members(String key);

    boolean isMember(String key, T value);

    T pop(String key);

    List<T> pop(String key, long count);

    Set<T> difference(List<String> keys);

    Set<T> difference(String key, String otherKey);

    Set<T> intersect(List<String> keys);

    Set<T> intersect(String key, String otherKey);

    Set<T> union(List<String> keys);

    Set<T> union(String key, String otherKey);
}
