package org.aibles.coreredis.service.impl;

import org.aibles.coreredis.service.BaseRedisHashService;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.Map;


public class BaseRedisHashServiceImpl<T> implements BaseRedisHashService<T> {

    private final HashOperations<String, String, T> hashOperations;

    public BaseRedisHashServiceImpl(RedisTemplate<String, T> redisTemplate) {
        this.hashOperations = redisTemplate.opsForHash();
    }

    @Override
    public void set(String key, String field, T value) {
        hashOperations.put(key, field, value);
    }

    @Override
    public T get(String key, String field) {
        return hashOperations.get(key, field);
    }

    @Override
    public Map<String, T> getAll(String key) {
        return hashOperations.entries(key);
    }

    @Override
    public void delete(String key, String field) {
        hashOperations.delete(key, field);
    }

    @Override
    public void increase(String key, String field, long count) {
        validateValueNumber(key, field);
        hashOperations.increment(key, field, count);
    }

    @Override
    public boolean isExist(String key, String field) {
        return hashOperations.hasKey(key, field);
    }

    @Override
    public long size(String key) {
        return hashOperations.size(key);
    }

    private void validateValueNumber(String key, String field) {
        try {
            Long value = (Long) get(key, field);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
