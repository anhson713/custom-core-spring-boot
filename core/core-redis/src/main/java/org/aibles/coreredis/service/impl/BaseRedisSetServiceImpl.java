package org.aibles.coreredis.service.impl;

import org.aibles.coreredis.service.BaseRedisSetService;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SetOperations;

import java.util.List;
import java.util.Set;

public class BaseRedisSetServiceImpl<T> implements BaseRedisSetService<T> {

    private final SetOperations<String, T> setOperations;

    public BaseRedisSetServiceImpl(RedisTemplate<String, T> redisTemplate) {
        this.setOperations = redisTemplate.opsForSet();
    }


    @Override
    public void add(String key, T value) {
        setOperations.add(key, value);
    }

    @Override
    public Set<T> members(String key) {
        return setOperations.members(key);
    }

    @Override
    public boolean isMember(String key, T value) {
        return setOperations.isMember(key, value);
    }

    @Override
    public T pop(String key) {
        return setOperations.pop(key);
    }

    @Override
    public List<T> pop(String key, long count) {
        return setOperations.pop(key, count);
    }

    @Override
    public Set<T> difference(List<String> keys) {
        return setOperations.difference(keys);
    }

    @Override
    public Set<T> difference(String key, String otherKey) {
        return setOperations.difference(key, otherKey);
    }

    @Override
    public Set<T> intersect(List<String> keys) {
        return setOperations.intersect(keys);
    }

    @Override
    public Set<T> intersect(String key, String otherKey) {
        return setOperations.intersect(key, otherKey);
    }

    @Override
    public Set<T> union(List<String> keys) {
        return setOperations.union(keys);
    }

    @Override
    public Set<T> union(String key, String otherKey) {
        return setOperations.union(key, otherKey);
    }
}
