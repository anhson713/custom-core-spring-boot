package org.aibles.coreredis.service;

import java.util.Map;

public interface BaseRedisHashService<T> {

    void set(String key, String field, T value);

    T get(String key, String field);

    Map<String, T> getAll(String key);

    void delete(String key, String field);

    void increase(String key, String field, long count);

    boolean isExist(String key, String field);

    long size(String key);
}
