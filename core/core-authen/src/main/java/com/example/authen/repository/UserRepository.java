package com.example.authen.repository;

import com.example.authen.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, String> {

    boolean existsByEmail(String email);

    @Modifying
    @Query("update User u set u.avatar = :fileName where u.id = :userId")
    void uploadAvatar(String userId, String fileName);

    @Query("select u.avatar from User u where u.id = :userId")
    Optional<String> findAvatar(String userId);
}
