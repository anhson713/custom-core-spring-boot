package com.example.authen.facade;

import com.example.authen.constant.Role;
import com.example.authen.dto.request.LoginRequest;
import com.example.authen.dto.request.RegisterRequest;
import com.example.authen.dto.response.AuthResponse;
import com.example.authen.entity.User;
import com.example.authen.exception.PasswordNotMatchException;
import com.example.authen.repository.projection.AuthInfoProjection;
import com.example.authen.service.AccountService;
import com.example.authen.service.AuthUserService;
import com.example.authen.service.TokenRedisService;
import com.example.authen.service.TokenService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;

import static com.example.authen.constant.CacheConstant.Token.ACCESS_TOKEN_KEY;
import static com.example.authen.constant.CacheConstant.Token.REFRESH_TOKEN_KEY;

@Slf4j
@RequiredArgsConstructor
public class AuthFacadeServiceImpl implements AuthFacadeService {

    private final PasswordEncoder passwordEncoder;
    private final AuthUserService authUserService;
    private final AccountService accountService;
    private final TokenService tokenService;
    private final TokenRedisService tokenRedisService;

    @Override
    @Transactional
    public AuthResponse login(LoginRequest request) {
        log.info("(login)request : {}", request);
        AuthInfoProjection authUserInfo = accountService.findAuthInfoByUsername(
            request.getUsername());
        validatePassword(request.getPassword(), authUserInfo.password());
        String accessToken = tokenService.generateAccessToken(authUserInfo.id(),
                authUserInfo.username(),
                authUserInfo.email(),
                authUserInfo.role().name());

        String refreshToken = tokenService.generateRefreshToken(authUserInfo.id(),
                authUserInfo.username(),
                authUserInfo.email());
        saveToken(authUserInfo.id(), accessToken, refreshToken);
        authenticate(authUserInfo, authUserInfo.id(), authUserInfo.username(), authUserInfo.role());

        return AuthResponse.of(request.getUsername(), accessToken, refreshToken);
    }

    private void authenticate(AuthInfoProjection authUserInfo, String userId, String username, Role role) {
        Collection<SimpleGrantedAuthority> grantedAuthorities = new ArrayList<>();
        grantedAuthorities.add(new SimpleGrantedAuthority(role.name()));
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                new UsernamePasswordAuthenticationToken(username,
                        userId,
                        grantedAuthorities);
        usernamePasswordAuthenticationToken.setDetails(authUserInfo);
        SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
    }

    private void validatePassword(String requestPassword, String encodedPassword) {
        log.info("(validatePassword)requestPassword : {}, encodedPassword : {}", requestPassword, encodedPassword);
        if (!passwordEncoder.matches(requestPassword, encodedPassword)) {
            throw new PasswordNotMatchException();
        }
    }

    @Override
    @Transactional
    public void register(RegisterRequest request) {
        log.info("(register)request : {}", request);
        User user = authUserService.create(request.getEmail());
        accountService.create(user.getId(), request.getUsername(), request.getPassword());
    }

    private void saveToken(String userId, String accessToken, String refreshToken) {
        tokenRedisService.set(ACCESS_TOKEN_KEY, userId, accessToken);
        tokenRedisService.set(REFRESH_TOKEN_KEY, userId, refreshToken);
    }
}
