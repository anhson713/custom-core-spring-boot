package com.example.authen.consumer;

import com.example.authen.dto.request.LoginRequest;
import com.example.authen.dto.request.RegisterRequest;
import com.example.authen.dto.response.BaseResponse;
import com.example.authen.facade.AuthFacadeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class AuthConsumer {

  private final AuthFacadeService service;

  @KafkaListener(topics = "${kafka.register.topic}", containerFactory = "kafkaListener", groupId = "${kafka.group-id}")
  public BaseResponse register(RegisterRequest request) {
    log.info("(register)request : {}", request);
    service.register(request);
    return BaseResponse.of(HttpStatus.CREATED.value());
  }

  @KafkaListener(topics = "${kafka.login.topic}", containerFactory = "kafkaListener", groupId = "${kafka.group-id}")
  public BaseResponse login(LoginRequest request) {
    log.info("(login)request : {}", request);
    return BaseResponse.of(HttpStatus.OK.value(), service.login(request));
  }
}
