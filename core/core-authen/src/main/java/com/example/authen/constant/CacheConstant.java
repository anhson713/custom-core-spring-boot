package com.example.authen.constant;

public class CacheConstant {

    public static class Token {

        public static final String ACCESS_TOKEN_KEY = "access-token";

        public static final String REFRESH_TOKEN_KEY = "refresh-token";
    }
}
