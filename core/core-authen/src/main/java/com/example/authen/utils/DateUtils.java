package com.example.authen.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateUtils {

    private DateUtils() {}

    public static final DateTimeFormatter DATE_INTEGER_FORMATTER = DateTimeFormatter.ofPattern("yyyyMMdd");

    public static LocalDate convertIntegerToLocalDate(Integer integerDate) {
        return LocalDate.parse(String.valueOf(integerDate), DATE_INTEGER_FORMATTER);
    }
}
