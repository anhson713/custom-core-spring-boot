package com.example.authen.controller;

import com.example.authen.dto.request.UpdateUserRequest;
import com.example.authen.dto.response.BaseResponse;
import com.example.authen.service.AuthUserService;
import com.example.authen.utils.SecurityUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.InputStreamSource;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import static com.example.authen.constant.ApiConstant.BaseUrl.USERS_API_URL;

@RequestMapping(USERS_API_URL)
@Slf4j
@RestController
@RequiredArgsConstructor
public class UserController {

    private final AuthUserService authUserService;

    @GetMapping("/self")
    @ResponseStatus(HttpStatus.OK)
    public BaseResponse get() {
        log.info("(get)");
        return BaseResponse.of(HttpStatus.OK.value(), authUserService.get(SecurityUtil.getUserId()));
    }

    @PutMapping("/self")
    @ResponseStatus(HttpStatus.OK)
    public BaseResponse update(@RequestBody @Validated UpdateUserRequest request) {
        log.info("(update)request : {}", request);
        return BaseResponse.of(HttpStatus.OK.value(), authUserService.update(SecurityUtil.getUserId(), request));
    }

    @GetMapping("/self-avatar")
    @ResponseStatus(HttpStatus.OK)
    public InputStreamSource getAvatar() {
        log.info("(getAvatar)");
        return authUserService.getAvatar(SecurityUtil.getUserId());
    }

    @PatchMapping(value = "/self-avatar", consumes = {"multipart/form-data"})
    @ResponseStatus(HttpStatus.OK)
    public void uploadAvatar(@RequestParam("file") MultipartFile file) {
        log.info("(uploadAvatar)file : {}", file);
        authUserService.uploadAvatar(SecurityUtil.getUserId(), file);
    }
}
