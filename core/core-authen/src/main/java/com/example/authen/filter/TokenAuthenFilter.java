package com.example.authen.filter;

import com.example.authen.repository.projection.AuthInfoProjection;
import com.example.authen.service.AccountService;
import com.example.authen.service.TokenService;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

@Component
@Slf4j
@RequiredArgsConstructor
public class TokenAuthenFilter extends OncePerRequestFilter {

    private final AccountService accountService;
    private final TokenService tokenService;

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {
        log.info("(doFilterInternal)request : {}, response : {}, filterChain : {}", request, response, filterChain);
        String authorizationHeader = request.getHeader("Authorization");
        if (Objects.isNull(authorizationHeader)) {
            filterChain.doFilter(request, response);
            return;
        }

        if (!authorizationHeader.contains("Bearer ")) {
            filterChain.doFilter(request, response);
            return;
        }

        String token = authorizationHeader.substring(7);
        String userId;

        try {
            userId = tokenService.getSubjectFromAccessToken(token);
        } catch (Exception e) {
            log.error("(doFilterInternal)exception : {}", e.getClass().getSimpleName());
            filterChain.doFilter(request, response);
            return;
        }
        log.info("(doFilterInternal)userId : {}", userId);
        if (Objects.nonNull(userId) && SecurityContextHolder.getContext().getAuthentication() == null) {
            if (tokenService.validateAccessToken(token, userId)) {
                log.info("(doFilterInternal)token : {}, userId : {}", token, userId);
                AuthInfoProjection authInfo = accountService.findAuthInfoById(userId);
                Collection<SimpleGrantedAuthority> grantedAuthorities = new ArrayList<>();
                grantedAuthorities.add(new SimpleGrantedAuthority(authInfo.role().name()));
                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                        new UsernamePasswordAuthenticationToken(authInfo.username(),
                                authInfo.id(),
                                grantedAuthorities);
                usernamePasswordAuthenticationToken.setDetails(authInfo);
                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
            }
        }

        filterChain.doFilter(request, response);
    }
}
