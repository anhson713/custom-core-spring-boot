package com.example.authen.constant;

import static com.example.authen.constant.ApiConstant.ApiPath.API_PATH;
import static com.example.authen.constant.ApiConstant.ApiPath.API_VERSION;
import static com.example.authen.constant.ApiConstant.Resource.AUTH_RESOURCE;
import static com.example.authen.constant.ApiConstant.Resource.USERS_RESOURCE;

public class ApiConstant {

    public static class ApiPath {
        public static final String API_PATH = "/api";
        public static final String API_VERSION = "/v1";
    }

    public static class Resource {

        public static final String AUTH_RESOURCE = "/auth";
        public static final String LOGIN_RESOURCE = "/login";
        public static final String REGISTER_RESOURCE = "/register";

        public static final String USERS_RESOURCE = "/user";
    }

    public static class BaseUrl {

        public static final String USERS_API_URL = API_PATH + API_VERSION + USERS_RESOURCE;

        public static final String AUTH_API_URL = API_PATH + API_VERSION + AUTH_RESOURCE;
    }
}
