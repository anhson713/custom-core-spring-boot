package com.example.authen.exception;

import org.trainingjava.coreexception.NotFoundException;

public class UserNotFoundException extends NotFoundException {

    public UserNotFoundException() {
        setCode("com.example.authen.exception.UserNotFoundException");
    }
}
