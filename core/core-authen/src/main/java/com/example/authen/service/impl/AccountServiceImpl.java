package com.example.authen.service.impl;

import com.example.authen.entity.Account;
import com.example.authen.exception.ExistedException;
import com.example.authen.repository.AccountRepository;
import com.example.authen.repository.projection.AuthInfoProjection;
import com.example.authen.service.AccountService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.trainingjava.coreexception.NotFoundException;

@Slf4j
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService {

  private final AccountRepository repository;

  private final PasswordEncoder passwordEncoder;
  @Override
  @Transactional
  public Account create(String userId, String username, String password) {
    log.info("(create)userId : {}, username : {}, password : {}", userId, username, password);
    validateExistUsername(username);
    Account account = Account.builder()
        .id(userId)
        .username(username)
        .password(passwordEncoder.encode(password))
        .build();
    return repository.save(account);
  }

  private void validateExistUsername(String username) {
    log.info("(validateExistUsername)username : {}", username);
    if (repository.existsByUsername(username)) {
      throw new ExistedException();
    }
  }

  @Override
  @Transactional(readOnly = true)
  public AuthInfoProjection findAuthInfoById(String userId) {
    log.info("(findAuthInfoById)userId : {}", userId);
    return repository.findAuthInfoById(userId).orElseThrow(NotFoundException::new);
  }

  @Override
  @Transactional(readOnly = true)
  public AuthInfoProjection findAuthInfoByUsername(String username) {
    log.info("(findAuthInfoByUsername)username : {}", username);
    return repository.findAuthInfoByUsername(username).orElseThrow(NotFoundException::new);
  }
}
