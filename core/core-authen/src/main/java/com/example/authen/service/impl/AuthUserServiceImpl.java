package com.example.authen.service.impl;

import com.example.authen.constant.Role;
import com.example.authen.dto.request.UpdateUserRequest;
import com.example.authen.dto.response.UserResponse;
import com.example.authen.entity.User;
import com.example.authen.exception.ExistedException;
import com.example.authen.mapper.UserMapper;
import com.example.authen.repository.UserRepository;
import com.example.authen.service.AuthUserService;
import com.example.authen.utils.ValidationUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.InputStreamSource;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.trainingjava.coreexception.NotFoundException;
import org.trainingjava.coreupload.service.FileUploadService;

import java.util.Objects;

@Slf4j
@RequiredArgsConstructor
public class AuthUserServiceImpl implements AuthUserService {

    private final UserRepository repository;

    private final FileUploadService fileUploadService;

    @Override
    @Transactional
    public User create(String email) {
        User user = User.builder()
                .email(email)
                .role(Role.USER)
                .build();
        user = repository.save(user);
        return user;
    }

    private void validateExistEmail(String email) {
        log.info("(validateExistEmail)email : {}", email);
        if (repository.existsByEmail(email)) {
            throw new ExistedException();
        }
    }

    @Transactional
    @Override
    public UserResponse update(String userId, UpdateUserRequest request) {
        log.info("(update)userId, : {}, request : {}", userId, request);
        User user = repository.findById(userId).orElse(null);
        ValidationUtils.validateNotFound((user != null));
        UserMapper.INSTANCE.updateEntity(user, request);
        user = repository.save(user);
        return UserMapper.INSTANCE.toDTO(user);
    }

    @Override
    @Transactional(readOnly = true)
    public UserResponse get(String userId) {
        log.info("(get)userId : {}", userId);
        User user = repository.findById(userId).orElse(null);
        ValidationUtils.validateNotFound((user != null));
        return UserMapper.INSTANCE.toDTO(user);
    }

    @Override
    public InputStreamSource getAvatar(String userId) {
        log.info("(getAvatar)userId : {}", userId);
        String fileName = repository.findAvatar(userId).orElseThrow(() -> {
            log.error("(getAvatar)userId : {}", userId);
            throw new NotFoundException();
        });
        return fileUploadService.get(fileName);
    }

    @Override
    @Transactional
    public void uploadAvatar(String userId, MultipartFile multipartFile) {
        log.info("(uploadAvatar)userId : {}, multipart : {}", userId, multipartFile);
        String fileName = repository.findAvatar(userId).orElse(null);
        if (Objects.nonNull(fileName)) {
            fileUploadService.delete(fileName);
        }
        fileName = fileUploadService.save(multipartFile);
        repository.uploadAvatar(userId, fileName);
    }
}
