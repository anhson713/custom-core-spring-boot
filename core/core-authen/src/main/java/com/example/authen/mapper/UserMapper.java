package com.example.authen.mapper;

import com.example.authen.dto.request.RegisterRequest;
import com.example.authen.dto.request.UpdateUserRequest;
import com.example.authen.dto.response.UserResponse;
import com.example.authen.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    void updateEntity(@MappingTarget User user, UpdateUserRequest request);

    UserResponse toDTO(User user);
}
