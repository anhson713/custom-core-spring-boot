package com.example.authen.exception;

import org.trainingjava.coreexception.BadRequestException;

public class ExistedException extends BadRequestException {

    public ExistedException() {
        setCode("com.example.authen.exception.ExistedException");
    }
}
