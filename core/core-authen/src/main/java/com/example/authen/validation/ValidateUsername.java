package com.example.authen.validation;

import jakarta.validation.Constraint;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import jakarta.validation.Payload;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.regex.Pattern;

@Target({ElementType.TYPE, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = ValidateUsername.UsernameValidation.class)
public @interface ValidateUsername {

    String message() default "Phone number is not invalid";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    class UsernameValidation implements ConstraintValidator<ValidateUsername, String> {

        @Override
        public boolean isValid(String username, ConstraintValidatorContext constraintValidatorContext) {
            final Pattern usernameRegex = Pattern.compile("[A-Za-z0-9]+");
            return usernameRegex.matcher(username).matches();
        }
    }
}
