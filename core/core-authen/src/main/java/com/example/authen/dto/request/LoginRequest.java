package com.example.authen.dto.request;

import com.example.authen.validation.ValidateUsername;
import lombok.Data;

@Data
public class LoginRequest {

    @ValidateUsername
    private String username;
    private String password;
}
