package com.example.authen.service.impl;

import com.example.authen.service.TokenService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Slf4j
@RequiredArgsConstructor
public class TokenServiceImpl implements TokenService {

    private final String accessTokenSecretKey;

    private final Long accessTokenLifetime;

    private final String refreshTokenSecretKey;

    private final Long refreshTokenLifetime;


    @Override
    public String generateAccessToken(String userId, String username, String email, String role) {
        log.info("(generateAccessToken)userId : {}, username : {}, email : {}, role : {}", userId, username, email, role);
        Map<String, Object> claims = new HashMap<>();
        claims.put("username", username);
        claims.put("email", email);
        claims.put("role", role);
        return generateToken(userId, claims, accessTokenSecretKey, accessTokenLifetime);
    }

    @Override
    public String getSubjectFromAccessToken(String token) {
        log.info("(getSubjectFromAccessToken)token : {}", token);
        return getClaim(token, Claims::getSubject, accessTokenSecretKey);
    }

    @Override
    public boolean validateAccessToken(String token, String userId) {
        log.info("(validateAccessToken)token : {}, userId : {}", token, userId);
        return validateToken(token, accessTokenSecretKey, userId);
    }

    @Override
    public String generateRefreshToken(String userId, String username, String email) {
        log.info("(generateRefreshToken)userId : {}, username : {}, email : {}", userId, username, email);
        Map<String, Object> claims = new HashMap<>();
        claims.put("username", username);
        claims.put("email", email);
        return generateToken(userId, claims, refreshTokenSecretKey, refreshTokenLifetime);
    }

    @Override
    public String getSubjectFromRefreshToken(String token) {
        log.info("(getSubjectFromRefreshToken)token : {}", token);
        return getClaim(token, Claims::getSubject, refreshTokenSecretKey);
    }

    @Override
    public boolean validateRefreshToken(String token, String userId) {
        log.info("(validateRefreshToken)token : {}, userId : {}", token, userId);
        return validateToken(token, refreshTokenSecretKey, userId);
    }

    private boolean validateToken(String token, String secretKey, String userId) {
        log.info("(validateToken)token : {}, secretKey : {}", token, secretKey);
        return getClaim(token, Claims::getSubject, secretKey).equals(userId) && isTokenInUsed(token, secretKey);
    }

    private boolean isTokenInUsed(String token, String secretKey) {
        log.info("(isTokenInUsed)token : {}, secretKey : {}", token, secretKey);
        return getClaim(token, Claims::getExpiration, secretKey).after(new Date());
    }

    private String generateToken(String subject, Map<String, Object> claims, String secretKey, Long tokenLifetime) {
        log.info("(generateToken)subject : {}, claims : {}, secretKey : {}, tokenLifetime : {}", subject, claims, secretKey, tokenLifetime);
        return Jwts.builder()
                .setClaims(claims)
                .setSubject(subject)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + tokenLifetime))
                .signWith(SignatureAlgorithm.HS256, secretKey)
                .compact();
    }

    private <T> T getClaim(String token, Function<Claims, T> claimsResolve, String secretKey) {
        log.info("(getClaim)token : {}, claimsResolve : {}, secretKey : {}", token, claimsResolve, secretKey);
        return claimsResolve.apply(getClaims(token, secretKey));
    }
    private Claims getClaims(String token, String secretKey) {
        log.info("(getClaims)token : {}, secretKey : {}", token, secretKey);
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody();
    }
}
