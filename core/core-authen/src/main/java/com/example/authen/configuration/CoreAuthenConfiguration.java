package com.example.authen.configuration;

import com.example.authen.facade.AuthFacadeService;
import com.example.authen.facade.AuthFacadeServiceImpl;
import com.example.authen.repository.AccountRepository;
import com.example.authen.repository.UserRepository;
import com.example.authen.service.AccountService;
import com.example.authen.service.AuthUserService;
import com.example.authen.service.TokenRedisService;
import com.example.authen.service.TokenService;
import com.example.authen.service.impl.AccountServiceImpl;
import com.example.authen.service.impl.AuthUserServiceImpl;
import com.example.authen.service.impl.TokenRedisServiceImpl;
import com.example.authen.service.impl.TokenServiceImpl;
import org.aibles.coreredis.configuration.EnableCoreRedis;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.client.RestTemplate;
import org.trainingjava.coreexceptionapi.configuration.EnableCoreExceptionApi;
import org.trainingjava.coreupload.configuration.EnableCoreFileUpload;
import org.trainingjava.coreupload.service.FileUploadService;

@Configuration
@EnableCoreExceptionApi
@EnableCoreFileUpload
@EnableCoreRedis
//@EnableDiscoveryClient
public class CoreAuthenConfiguration {

    @Value("${jwt.access-token.secret-key}")
    private String accessTokenSecretKey;

    @Value("${jwt.access-token.life-time}")
    private Long accessTokenLifeTime;

    @Value("${jwt.refresh-token.secret-key}")
    private String refreshTokenSecretKey;

    @Value("${jwt.refresh-token.life-time}")
    private Long refreshTokenLifeTime;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public TokenService tokenService() {
        return new TokenServiceImpl(
                accessTokenSecretKey,
                accessTokenLifeTime,
                refreshTokenSecretKey,
                refreshTokenLifeTime);
    }

    @Bean
    public AuthUserService authUserService(
            UserRepository repository,
            FileUploadService fileUploadService) {
        return new AuthUserServiceImpl(repository, fileUploadService);
    }

    @Bean
    public AuthFacadeService authFacadeService(PasswordEncoder passwordEncoder,
                                               AuthUserService authUserService,
                                               AccountService accountService,
                                               TokenService tokenService,
                                               TokenRedisService tokenRedisService) {
        return new AuthFacadeServiceImpl(
            passwordEncoder, authUserService, accountService, tokenService, tokenRedisService);
    }

    @Bean
    public TokenRedisService tokenRedisService(RedisTemplate<String, String> redisTemplate) {
        return new TokenRedisServiceImpl(redisTemplate);
    }

    @Bean
    public AccountService accountService(AccountRepository repository, PasswordEncoder passwordEncoder) {
        return new AccountServiceImpl(repository, passwordEncoder);
    }
}
