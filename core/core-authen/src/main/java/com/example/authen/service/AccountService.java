package com.example.authen.service;


import com.example.authen.entity.Account;
import com.example.authen.repository.projection.AuthInfoProjection;

public interface AccountService {

  Account create(String userId, String username, String password);

  AuthInfoProjection findAuthInfoById(String userId);

  AuthInfoProjection findAuthInfoByUsername(String username);
}
