package com.example.authen.utils;

import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityUtil {

    public static String getUserId() {
        return (String) SecurityContextHolder.getContext().getAuthentication().getCredentials();
    }

    public static String getUsername() {
        return (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}
