package com.example.authen.repository;

import com.example.authen.entity.Account;
import com.example.authen.repository.projection.AuthInfoProjection;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface AccountRepository extends JpaRepository<Account, String> {

  boolean existsByUsername(String username);

  @Query("select new com.example.authen.repository.projection.AuthInfoProjection(a.id, a.username, a.password, u.email, u.role) from Account a inner join User u on a.id = u.id where a.id = :id" )
  Optional<AuthInfoProjection> findAuthInfoById(String id);

  @Query(value = "select new com.example.authen.repository.projection.AuthInfoProjection(a.id, a.username, a.password, u.email, u.role) from Account a inner join User u on a.id = u.id where a.username = :username")
  Optional<AuthInfoProjection> findAuthInfoByUsername(String username);
}
