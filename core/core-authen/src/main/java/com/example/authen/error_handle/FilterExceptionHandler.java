package com.example.authen.error_handle;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.HashMap;

@Component
@Slf4j
public class FilterExceptionHandler implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest request,
                         HttpServletResponse response,
                         AuthenticationException authException) throws IOException, ServletException {
        log.info("(commence)request : {}, response : {}", request, response);
        var error = new HashMap<String, Object>();
        error.put("status", 401);
        error.put("timestamp", System.currentTimeMillis());
        error.put("message", "Unauthenticated");
        response.sendError(401, new ObjectMapper().writeValueAsString(error));
    }
}
