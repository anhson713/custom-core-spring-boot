package com.example.authen.service;

import com.example.authen.dto.request.UpdateUserRequest;
import com.example.authen.dto.response.UserResponse;
import com.example.authen.entity.User;
import org.springframework.core.io.InputStreamSource;
import org.springframework.web.multipart.MultipartFile;

public interface AuthUserService {

    User create(String email);

    UserResponse update(String userId, UpdateUserRequest request);

    UserResponse get(String userId);

    InputStreamSource getAvatar(String userId);

    void uploadAvatar(String userId, MultipartFile multipartFile);
}
