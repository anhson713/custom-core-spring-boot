package com.example.authen.facade;

import com.example.authen.dto.request.LoginRequest;
import com.example.authen.dto.request.RegisterRequest;
import com.example.authen.dto.response.AuthResponse;

public interface AuthFacadeService {

    AuthResponse login(LoginRequest request);

    void register(RegisterRequest request);
}
