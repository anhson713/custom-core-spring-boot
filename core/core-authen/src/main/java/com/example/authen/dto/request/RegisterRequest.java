package com.example.authen.dto.request;

import com.example.authen.exception.PasswordNotMatchException;
import com.example.authen.validation.ValidateEmail;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class RegisterRequest {

    @NotBlank
    private String username;

    @NotBlank
    private String password;

    @NotBlank
    private String confirmPassword;

    @ValidateEmail
    private String email;

    public void validateConfirmPassword() {
        log.info("(validateConfirmPassword)");
        if (!password.equals(confirmPassword)) {
            log.error("(validateConfirmPassword) --> PASSWORD NOT MATCH EXCEPTION");
            throw new PasswordNotMatchException();
        }
    }
}
