package com.example.authen.service;

public interface TokenService {

    String generateAccessToken(String userId, String username, String email, String role);

    String getSubjectFromAccessToken(String token);

    boolean validateAccessToken(String token, String userId);

    String generateRefreshToken(String userId, String username, String email);

    String getSubjectFromRefreshToken(String token);

    boolean validateRefreshToken(String token, String userId);
}
