package com.example.authen.dto.response;

import lombok.Data;

import java.time.Instant;

@Data
public class BaseResponse {

    public int status;

    public Instant timestamp;

    public Object data;

    public static BaseResponse of(int status, Object data) {
        BaseResponse response = new BaseResponse();
        response.setStatus(status);
        response.setTimestamp(Instant.now());
        response.setData(data);
        return response;
    }

    public static BaseResponse of(int status) {
        return BaseResponse.of(status, null);
    }
}