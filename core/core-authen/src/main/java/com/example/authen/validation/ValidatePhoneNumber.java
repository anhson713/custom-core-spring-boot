package com.example.authen.validation;

import jakarta.validation.Constraint;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import jakarta.validation.Payload;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.regex.Pattern;

@Target({ElementType.TYPE, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = ValidatePhoneNumber.PhoneNumberValidation.class)
public @interface ValidatePhoneNumber {

    String message() default "Phone number is not invalid";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    class PhoneNumberValidation implements ConstraintValidator<ValidatePhoneNumber, String> {

        @Override
        public boolean isValid(String phoneNumber, ConstraintValidatorContext constraintValidatorContext) {
            final Pattern phoneRegex = Pattern.compile("(0)(3|5|7|8|9)+([0-9]{8})\\b");
            return phoneRegex.matcher(phoneNumber).matches();
        }
    }
}
