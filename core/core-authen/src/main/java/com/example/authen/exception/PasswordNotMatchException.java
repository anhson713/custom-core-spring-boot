package com.example.authen.exception;

import org.trainingjava.coreexception.BadRequestException;

public class PasswordNotMatchException extends BadRequestException {

    public PasswordNotMatchException() {
        setCode("com.example.authen.exception.PasswordNotMatchException");
    }
}
