package com.example.authen.validation;

import com.example.authen.utils.ValidationUtils;
import jakarta.validation.Constraint;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import jakarta.validation.Payload;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Documented
@Constraint(validatedBy = ValidateDateInteger.DateIntegerValidator.class)
public @interface ValidateDateInteger {


    String message() default "Date is not invalid";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    class DateIntegerValidator implements ConstraintValidator<ValidateDateInteger, Integer> {

        @Override
        public boolean isValid(Integer integer, ConstraintValidatorContext constraintValidatorContext) {
            return ValidationUtils.validateDateInteger(integer);
        }
    }
}
