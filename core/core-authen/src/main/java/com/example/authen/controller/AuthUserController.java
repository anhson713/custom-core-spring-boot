package com.example.authen.controller;

import static com.example.authen.constant.ApiConstant.BaseUrl.AUTH_API_URL;
import static com.example.authen.constant.ApiConstant.Resource.LOGIN_RESOURCE;
import static com.example.authen.constant.ApiConstant.Resource.REGISTER_RESOURCE;

import com.example.authen.dto.request.LoginRequest;
import com.example.authen.dto.request.RegisterRequest;
import com.example.authen.dto.response.BaseResponse;
import com.example.authen.facade.AuthFacadeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping(value = AUTH_API_URL)
@Slf4j
@RequiredArgsConstructor
@RestController
public class AuthUserController {

    private final AuthFacadeService authFacadeService;

    @PostMapping(LOGIN_RESOURCE)
    @ResponseStatus(HttpStatus.OK)
    public BaseResponse login(@RequestBody @Validated LoginRequest request) {
        log.info("(login)request : {}", request);
        return BaseResponse.of(HttpStatus.OK.value(),authFacadeService.login(request));
    }

    @PostMapping(REGISTER_RESOURCE)
    @ResponseStatus(HttpStatus.CREATED)
    public BaseResponse register(@RequestBody @Validated RegisterRequest request) {
        log.info("(register)request : {}", request);
        request.validateConfirmPassword();
        authFacadeService.register(request);
        return BaseResponse.of(HttpStatus.CREATED.value());
    }
}
