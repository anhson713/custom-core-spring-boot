package com.example.authen.dto.request;

import com.example.authen.constant.Gender;
import com.example.authen.validation.ValidateDateInteger;
import com.example.authen.validation.ValidateEnum;
import com.example.authen.validation.ValidatePhoneNumber;
import lombok.Data;

@Data
public class UpdateUserRequest {

    @ValidateEnum(enumClazz = Gender.class)
    private String gender;

    @ValidatePhoneNumber
    private String phone;

    private String address;

    @ValidateDateInteger
    private Integer dob;
}
