package com.example.authen.repository.projection;

import com.example.authen.constant.Role;
import lombok.Value;



public record AuthInfoProjection(String id, String username, String password, String email, Role role) {
}
