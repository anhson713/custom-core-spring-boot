package com.example.authen.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor(staticName = "of")
public class AuthResponse {

    private String username;

    private String accessToken;

    private String refreshToken;
}
