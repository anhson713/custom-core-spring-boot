package com.example.authen.service;

import org.aibles.coreredis.service.BaseRedisHashService;

public interface TokenRedisService extends BaseRedisHashService<String> {
}
