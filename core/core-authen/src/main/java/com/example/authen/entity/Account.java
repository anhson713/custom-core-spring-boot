package com.example.authen.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "account")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Account extends BaseEntity {

  @Id
  private String id;

  @Column(nullable = false)
  private String username;

  @Column(nullable = false)
  private String password;
}
