package com.example.authen.service.impl;

import com.example.authen.service.TokenRedisService;
import org.aibles.coreredis.service.impl.BaseRedisHashServiceImpl;
import org.springframework.data.redis.core.RedisTemplate;

public class TokenRedisServiceImpl extends BaseRedisHashServiceImpl<String> implements TokenRedisService {
    public TokenRedisServiceImpl(RedisTemplate<String, String> redisTemplate) {
        super(redisTemplate);
    }
}
