package com.example.authen.utils;

import lombok.extern.slf4j.Slf4j;
import org.trainingjava.coreexception.NotFoundException;

import java.time.LocalDate;
import java.util.Objects;

@Slf4j
public class ValidationUtils {

    private ValidationUtils() {}

    public static boolean validateDateInteger(Integer integerDate) {
        log.info("(validateDateInteger)integerDate : {}", integerDate);
        if (Objects.isNull(integerDate)) {
            return true;
        }
        try {
            LocalDate localDate = DateUtils.convertIntegerToLocalDate(integerDate);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public static void validateNotFound(boolean isExist) {
        log.info("(validateExist)isExist : {}", isExist);
        if (!isExist) {
            log.error("(validateExist)isExist : {} --> NOT FOUND EXCEPTION", isExist);
            throw new NotFoundException();
        }
    }
}
