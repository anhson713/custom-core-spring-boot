package com.example.authen.dto.response;

import com.example.authen.constant.Gender;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class UserResponse {

    @JsonProperty("username")
    private String username;

    @JsonProperty("email")
    private String email;

    @JsonProperty("gender")
    private Gender gender;

    @JsonProperty("dob")
    private Integer dob;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("address")
    private String address;
}
