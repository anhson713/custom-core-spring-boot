package org.trainingjava.coreexception;

import java.util.HashMap;
import java.util.Map;

public class BaseException extends RuntimeException {

    private int status;

    private String code;

    private Map<String, String> params;
    public BaseException() {

    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    public void addParams(String key, String value) {
        if (this.params == null) {
            this.params = new HashMap<>();
        }
        params.put(key, value);
    }
}
