package org.trainingjava.coreexception;

import org.springframework.http.HttpStatus;
import org.trainingjava.coreexception.BaseException;

public class NotFoundException extends BaseException {

    public NotFoundException() {
        setStatus(HttpStatus.NOT_FOUND.value());
        setCode("org.trainingjava.coreexception.exception.NotFoundException");
    }
}
