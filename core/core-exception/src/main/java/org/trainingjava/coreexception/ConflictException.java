package org.trainingjava.coreexception;

import org.springframework.http.HttpStatus;
import org.trainingjava.coreexception.BaseException;

public class ConflictException extends BaseException {

    public ConflictException() {
        setStatus(HttpStatus.CONFLICT.value());
        setCode("org.trainingjava.coreexception.exception.ConflictException");
    }
}
