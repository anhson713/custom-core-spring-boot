package org.trainingjava.coreexception;

import org.springframework.http.HttpStatus;
import org.trainingjava.coreexception.BaseException;

public class InternalServerException extends BaseException {

    public InternalServerException() {
        setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        setCode("org.trainingjava.coreexception.exception.InternalServerException");
    }
}
