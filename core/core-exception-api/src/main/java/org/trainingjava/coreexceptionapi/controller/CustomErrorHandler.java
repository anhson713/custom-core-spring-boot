package org.trainingjava.coreexceptionapi.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.trainingjava.coreexception.BaseException;
import org.trainingjava.coreexceptionapi.model.ErrorResponse;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
@Slf4j
public class CustomErrorHandler {

    @ExceptionHandler(BaseException.class)
    public ResponseEntity<ErrorResponse> handle(BaseException exception) {
        log.info("(handle)exception : {}", exception.getClass().getSimpleName());
        return ResponseEntity
                .status(exception.getStatus())
                .body(ErrorResponse.of(exception.getStatus()
                        , Instant.now(),
                        exception.getCode()));
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorResponse> handle(MethodArgumentNotValidException exception) {
        log.info("(handle)exception : {}", exception.getClass().getSimpleName());
        Map<String, String> errorMap = new HashMap<>();
        exception.getBindingResult()
                .getAllErrors()
                .forEach(error -> {
                    errorMap.put(((FieldError) error).getField(), error.getDefaultMessage());
                });

        return ResponseEntity.status(HttpStatus.BAD_REQUEST.value())
                .body(ErrorResponse.of(
                        HttpStatus.BAD_REQUEST.value(),
                        Instant.now(),
                        errorMap));
    }
}
